from .aspect import Square, Orientation, Aspect, Aspects, Resolution, Resolutions, AspectToRes, ThumbOrder
from .work import Wallpaper, Work, Artist
