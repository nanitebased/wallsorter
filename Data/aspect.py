from enum import Enum, unique
from math import isclose
from typing import Optional, Union


@unique
class Orientation(Enum):
    Portrait = 0
    Landscape = 1

    @property
    def Initial(self) -> str:
        return 'p' if self.name.startswith('P') else 'l'


class Square:
    def __init__(self, w: float, h: float):
        self.Width = w
        self.Height = h
        self.Ratio = w/h
        self.Orientation = Orientation.Landscape if w > h else Orientation.Portrait

    def __str__(self):
        return f"{self.Width}x{self.Height}({self.Ratio:.3f})"

    def __repr__(self):
        return self.__str__()


class Aspect(Square):
    def __init__(self, w: float, h: float):
        super().__init__(w, h)
        self.Name = f'A{w}_{h}'.replace('.', '')

    def IsMatch(self, other: Union[float, Square]) -> bool:
        if isinstance(other, Square):
            other = other.Ratio
        return isclose(other, self.Ratio, abs_tol=1e-2)


@unique
class Aspects(Enum):
    A3_2 = Aspect(3, 2)
    A16_10 = Aspect(16, 10)
    A16_9 = Aspect(16, 9)
    A185_9 = Aspect(18.5, 9)

    A2_3 = Aspect(2, 3)
    A10_16 = Aspect(10, 16)
    A9_16 = Aspect(9, 16)
    A9_185 = Aspect(9, 18.5)

    @staticmethod
    def FindMatch(sq: Square) -> Optional[Aspect]:
        for a in Aspects:
            if a.value.IsMatch(sq): return a.value
        return None

    @staticmethod
    def FromShortId(shortId: str) -> Optional[Aspect]:
        try:
            return Aspects[shortId].value
        except KeyError:
            return Aspects.FromOldId(shortId)

    @staticmethod
    def FromOldId(oldId: str) -> Optional[Aspect]:
        if oldId == 'l1': return Aspects.A3_2.value
        if oldId == 'l2': return Aspects.A16_9.value
        if oldId == 'l3': return Aspects.A16_10.value
        if oldId == 'l4': return Aspects.A185_9.value
        if oldId == 'p4': return Aspects.A9_185.value
        if oldId == 'p3': return Aspects.A9_16.value
        if oldId == 'p2': return Aspects.A10_16.value
        if oldId == 'p1': return Aspects.A2_3.value
        return None


class Resolution(Square):
    def __init__(self, w: int, h: int):
        super().__init__(w, h)
        self.Aspect = Aspects.FindMatch(self)
        self.Name = f'R{w}x{h}'


@unique
class Resolutions(Enum):
    R1920x1080 = Resolution(1920, 1080)
    R1920x1200 = Resolution(1920, 1200)
    R2160x1440 = Resolution(2160, 1440)
    R2304x1440 = Resolution(2304, 1440)
    R2560x1440 = Resolution(2560, 1440)
    R2960x1440 = Resolution(2960, 1440)

    R1080x1920 = Resolution(1080, 1920)
    R1200x1920 = Resolution(1200, 1920)
    R1440x2160 = Resolution(1440, 2160)
    R1440x2304 = Resolution(1440, 2304)
    R1440x2560 = Resolution(1440, 2560)
    R1440x2960 = Resolution(1440, 2960)

    @staticmethod
    def ExactMatch(sq: Square) -> Optional[Resolution]:
        for r in [r.value for r in Resolutions]:
            if r.Width == sq.Width and r.Height == sq.Height:
                return r
        return None

    @staticmethod
    def SmallerMatch(sq: Square) -> Optional[Resolution]:
        matches = []
        for r in [r.value for r in Resolutions]:
            if r.Aspect.IsMatch(sq):
                matches.append(r)
        matches = list(filter(lambda x: x.Width <= sq.Width, matches))  # Remove all entries bigger than the parameter
        matches.sort(key=lambda x: x.Width)  # Sort and return the biggest
        return matches[-1] if len(matches) != 0 else None


AspectToRes = {
    Aspects.A3_2: [Resolutions.R2160x1440],
    Aspects.A16_10: [Resolutions.R2304x1440, Resolutions.R1920x1200],
    Aspects.A16_9: [Resolutions.R2560x1440, Resolutions.R1920x1080],
    Aspects.A185_9: [Resolutions.R2960x1440],

    Aspects.A2_3: [Resolutions.R1440x2160],
    Aspects.A10_16: [Resolutions.R1440x2304, Resolutions.R1200x1920],
    Aspects.A9_16: [Resolutions.R1440x2560, Resolutions.R1080x1920],
    Aspects.A9_185: [Resolutions.R1440x2960],
}


ThumbOrder = [
    Resolutions.R2160x1440, Resolutions.R2304x1440, Resolutions.R1920x1200,
    Resolutions.R2560x1440, Resolutions.R1920x1080, Resolutions.R2960x1440,
    Resolutions.R1440x2160, Resolutions.R1440x2304, Resolutions.R1200x1920,
    Resolutions.R1440x2560, Resolutions.R1080x1920, Resolutions.R2160x1440
]