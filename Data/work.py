from PIL import Image, ImageChops
from os.path import abspath, basename, join, isdir, isfile, dirname, exists
from os import listdir, makedirs, rename
from .aspect import Aspect, Aspects, Square, Resolution, Resolutions, ThumbOrder, AspectToRes
from typing import Dict, Optional, Tuple, List
from string import digits, punctuation

overlayTop = Image.open('overlay_top.png').resize((1440, 80), Image.NEAREST)
overlayBottom = Image.open('overlay_bottom.png').resize((1440, 150), Image.NEAREST)


class Wallpaper:
    def __init__(self, path: str):
        self.Path = abspath(path)
        self.File = basename(self.Path)
        self.Folder = dirname(self.Path)
        self.Artist, self.Work, self.AspectId = self.File.replace('.png', '').split()
        # Lazy loading
        self._size = None
        self._aspect = None
        self._resolution = None

    def lazyLoad(self):
        img = Image.open(self.Path)
        self._size = Square(*img.size)
        self._aspect = Aspects.FindMatch(self._size)
        self._resolution = Resolutions.ExactMatch(self._size)

    @property
    def Size(self) -> Square:
        if self._size is None:
            self.lazyLoad()
        return self._size

    @property
    def Aspect(self) -> Aspect:
        if self._aspect is None:
            self.lazyLoad()
        return self._aspect

    @property
    def Resolution(self) -> Resolution:
        if self._resolution is None:
            self.lazyLoad()
        return self._resolution


class Work:
    def __init__(self, artist: str, workId: str, collection: str):
        self.Artist = artist
        self.Work = workId
        self.Collection = collection
        self.Wallpapers: Dict[str, Wallpaper] = {}

    def Add(self, wallpaper: Wallpaper):
        if wallpaper.Resolution is None:
            raise RuntimeError("Cannot index with a non-standard wallpaper size")
        self.Wallpapers[wallpaper.Resolution.Name] = wallpaper

    def MakeThumbs(self, forced=False):
        def _bestThumbWallpaper():
            for resolution in ThumbOrder:
                if resolution.value.Name in self.Wallpapers.keys():
                    return self.Wallpapers[resolution.value.Name]
            return None

        def _generate(wallpaper: Wallpaper, target: str, size: int):
            img = Image.open(wallpaper.Path)  # type: Image
            img.thumbnail((size, size))
            if not exists(dirname(target)):
                makedirs(dirname(target), exist_ok=True)
            img.save(target, 'JPEG')

        # Generate main thumbnail
        target = join('static', self.MainThumbPath)
        if forced or not exists(target):
            wallpaper = _bestThumbWallpaper()
            _generate(wallpaper, target, 256)
            print(f"Generated main thumb for {wallpaper.File}")

        # All small thumbnails
        for wallpaper in self.Wallpapers.values():
            aspectId = wallpaper.Resolution.Aspect.Name
            target = join('static', 'minithumbs', self.Artist, self.Work, f'{aspectId}.jpg')
            if forced or not exists(target):
                _generate(wallpaper, target, 128)
                print(f"Generated thumb for {wallpaper.File} - {aspectId}")
                if wallpaper.AspectId != aspectId:
                    target = join(wallpaper.Folder, wallpaper.File.replace(wallpaper.AspectId, aspectId))
                    rename(wallpaper.Path, target)
                    print(f"Renamed '{wallpaper.Path}' to '{target}'")

    @property
    def MainThumbPath(self) -> str:
        return f"thumbs/{self.Artist}/{self.Work}.jpg"

    @property
    def AllAspectsData(self) -> Dict[str, Tuple[str, str]]:
        """One pair of (<Image size>, <Thumb path>) per aspect ratio"""
        res = {}

        for aspect in [a.value for a in AspectToRes.keys()]:
            aspectWall = None
            for wallpaper in self.Wallpapers.values():
                if wallpaper.Aspect.IsMatch(aspect):
                    aspectWall = wallpaper
            if aspectWall is None:
                res[aspect.Name] = (None, None)
            else:
                res[aspect.Name] = (str(aspectWall.Size),
                                    f"minithumbs/{self.Artist}/{self.Work}/{aspectWall.Resolution.Aspect.Name}.jpg")
        return res


class Artist:
    def __init__(self, artist: str):
        self.Artist = artist
        self.Works: Dict[str, Work] = {}

    def Add(self, wallpaper: Wallpaper, collection: str):
        if wallpaper.Work not in self.Works.keys():
            workInstance = Work(wallpaper.Artist, wallpaper.Work, collection)
            self.Works[wallpaper.Work] = workInstance
        else:
            workInstance = self.Works[wallpaper.Work]

        workInstance.Add(wallpaper)

    @property
    def LastWork(self) -> Optional[str]:
        return max([w.Work for w in self.Works.values()], default=None)

    @property
    def NextWork(self) -> str:
        last = self.LastWork
        next = int(last) + 1 if last is not None else 0
        return f'{next:04d}'

    def MoveWorks(self, collectionPath: str, works: List[int]) -> Tuple[str, str]:
        """Returns (<Message>, <[is-success|is-warning|is-danger]>)"""
        totalWorks = totalFiles = 0
        for workId in [f'{w:04d}' for w in works]:
            work = self.Works[workId]
            print(f"Moving work {workId}")
            for wallpaper in work.Wallpapers.values():
                try:
                    source = wallpaper.Path
                    target = abspath(join(collectionPath, wallpaper.Aspect.Name, wallpaper.File))
                    if not exists(dirname(target)):
                        makedirs(dirname(target), exist_ok=True)
                    print(f"  Source: {source}")
                    print(f"  Target: {target}")
                    rename(source, target)
                    totalFiles += 1
                except Exception as e:
                    return f"Exception while moving works: {e} [Moved {totalWorks} works, {totalFiles} files]", \
                           "is-danger"
            totalWorks += 1
        return f"Moved {totalWorks} works, {totalFiles} files", "is-info"

    def ProcessNewWork(self, collectionPath: str, entrypoint: str, work: int) -> Tuple[str, str]:
        """Returns (<Message>, <[is-success|is-warning|is-danger]>)"""
        workId = f'{work:04d}'
        processed = 0
        overrides = False

        try:
            for file in [join(entrypoint, f) for f in listdir(entrypoint) if isfile(join(entrypoint, f))]:
                if not file.endswith(".png"): continue
                print(f"Processing file '{file}'")

                img: Image = Image.open(file)
                size = Square(*img.size)
                resolution = Resolutions.SmallerMatch(size)
                if resolution is None:
                    raise RuntimeError(f"Unable to find valid target resolution for size {size}")

                print(f"  Size: {size} - Target Resolution: {resolution}")
                if size.Width != resolution.Width or size.Height != resolution.Height:
                    img = img.resize((resolution.Width, resolution.Height), Image.ANTIALIAS)

                for aspect in [Aspects.A10_16, Aspects.A9_16, Aspects.A9_185]:
                    if resolution.Aspect.IsMatch(aspect.value):
                        overlay = Image.new('RGB', (resolution.Width, resolution.Height), 'white')
                        overlay.paste(overlayTop)
                        overlay.paste(overlayBottom, (0, resolution.Height - 150))
                        img = ImageChops.multiply(img, overlay)

                target = join(collectionPath, resolution.Aspect.Name,
                              f'{self.Artist} {workId} {resolution.Aspect.Name}.png')
                print(f"  Target: {target}")
                if exists(target):
                    overrides = True
                elif not exists(dirname(target)):
                    makedirs(dirname(target), exist_ok=True)

                img.save(target)
                rename(file, file + '_')
                processed += 1

        except Exception as e:
            return f"{e} [Processed {processed}]", 'is-danger'

        if overrides:
            return f"Updated work {workId} - {processed} processed", "is-warning"
        else:
            return f"Processed work {workId} - {processed} processed", "is-success"


    @staticmethod
    def GetArtist(collections: Dict, artist: str) -> Optional["Artist"]:
        instances = Artist.GetInstances(collections, artist[0], artist[0], artist)
        return instances.get(artist, None)

    @staticmethod
    def GetNamesOnly(collections: Dict, firstChar: str, lastChar: str) -> List[str]:
        instances = Artist.GetInstances(collections, firstChar, lastChar, None, skipFileLoad=True)
        return list(sorted(instances.keys()))

    @staticmethod
    def GetRangeInstances(collections: Dict, firstChar: str, lastChar: str) -> Dict[str, "Artist"]:
        return Artist.GetInstances(collections, firstChar, lastChar, None)

    @staticmethod
    def GetInstances(collections: Dict, firstChar: str, lastChar: str, artist: Optional[str],
                     skipFileLoad=False) -> Dict[str, "Artist"]:
        firstChar = firstChar[0]
        lastChar = lastChar[0]

        if firstChar in [*digits, *punctuation]:
            firstChar = 'a'
            hasDigits = True
        else:
            hasDigits = False

        chars = [chr(c) for c in range(ord(firstChar), ord(lastChar) + 1)]
        if hasDigits:
            chars.extend(digits[:])
            chars.extend(punctuation[:])

        artists: Dict[str, Artist] = {}
        for collection, data in collections.items():
            path = data["Path"]

            for folder, subfolder in [(join(path, d), d) for d in listdir(path) if isdir(join(path, d))]:
                if subfolder.startswith('.'): continue
                for file in [join(folder, f) for f in listdir(folder) if isfile(join(folder, f))]:
                    if not file.endswith('.png'): continue
                    wallpaper = Wallpaper(file)
                    if wallpaper.Artist[0] not in chars: continue
                    if artist is not None and wallpaper.Artist != artist: continue

                    if wallpaper.Artist not in artists.keys():
                        artists[wallpaper.Artist] = Artist(wallpaper.Artist)

                    if not skipFileLoad:
                        artists[wallpaper.Artist].Add(wallpaper, collection)

        return artists
