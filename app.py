from flask import Flask, render_template, abort, request, flash, redirect, url_for
import yaml
from Data import Artist
from flask_wtf import FlaskForm


with open('config.yml', 'r') as stream:
    config = yaml.safe_load(stream)

app = Flask(__name__)
app.config['SECRET_KEY'] = config['SecretKey']
collections = config['Collections']
entrypoint = config['Entrypoint']

colors = {}
for name, data in collections.items():
    colors[name] = data["Color"]


@app.route('/')
def index():
    artists = Artist.GetNamesOnly(collections, '0', 'z')
    return render_template('index.html', artists=artists)


@app.route('/artist/<string:name>', methods=['GET', 'POST'])
def artist(name: str):
    form = FlaskForm()
    forced = False

    instance = Artist.GetArtist(collections, name)
    if instance is None:
        instance = Artist(name)
        if not form.is_submitted():  # Avoid double messages when submitting
            flash(f"No works found for artist '{name}'.", 'is-info')

    if form.validate_on_submit():
        if 'reload' in request.form.keys():
            forced = True
            flash('Regenerated thumbs', 'is-info')
        else:
            work = int(request.form['work'])
            collection = request.form["targetCollection"]
            collectionPath = collections[collection]["Path"]
            message, category = "Unrecognized action", "is-danger"

            if 'process' in request.form.keys():
                message, category = instance.ProcessNewWork(collectionPath, entrypoint, work)
            elif 'move' in request.form.keys():
                selected = [int(c.replace('_checkbox', '')) for c in request.form.keys() if '_checkbox' in c]
                message, category = instance.MoveWorks(collectionPath, selected)
            flash(message, category)

    for work in instance.Works.values():
        work.MakeThumbs(forced)

    if form.is_submitted():
        return redirect(url_for('artist', name=name))
    else:
        return render_template('artist.html', artist=instance, form=form, nextWork=int(instance.NextWork),
                               colors=colors)


if __name__ == '__main__':
    app.run()
